package khang.blinkfeed.activity;

import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.Toast;

import khang.blinkfeed.R;

// The activity show a form to fill a feed provider
public class AddFeedActivity extends ActionBarActivity {
    @Override protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_add_feed);
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feed_preference, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menu_action_add) {
            toastMessage("new category");
        } else if (itemId == R.id.menu_action_save) {

        } else if (itemId == R.id.menu_action_delete) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void toastMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }
}