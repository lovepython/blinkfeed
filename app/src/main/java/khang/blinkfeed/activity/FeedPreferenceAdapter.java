package khang.blinkfeed.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;
import java.util.ArrayList;

import android.widget.TextView;
import android.widget.CheckedTextView;
import khang.blinkfeed.preference.FeedPreference;

public class FeedPreferenceAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private List<FeedPreference> dataItems = new ArrayList<FeedPreference>();

    public FeedPreferenceAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override public int getCount() {
        return dataItems.size();
    }

    @Override public Object getItem(int position) {
        return dataItems.get(position);
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View v, ViewGroup parent) {
        FeedPreference item = (FeedPreference)getItem(position);
        if (v == null) {
            if (item.getType()) {
                v = mLayoutInflater.inflate(android.R.layout.simple_list_item_checked, null);
            } else {
                v = mLayoutInflater.inflate(android.R.layout.simple_list_item_2, null);
            }
        }
        if (item.getType()) {
            CheckedTextView text = (CheckedTextView)v.findViewById(android.R.id.text1);
        } else {
            TextView text1 = (TextView)v.findViewById(android.R.id.text1);
            TextView text2 = (TextView)v.findViewById(android.R.id.text2);
            if (item.getKey().equals("category")) {

            } else if (item.getKey().equals("url")) {

            }
        }
        return v;
    }
}