package khang.blinkfeed.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;

import khang.blinkfeed.Feed;
import khang.blinkfeed.R;
import khang.blinkfeed.database.Database;
import khang.blinkfeed.utils.SQLCreateTableBuilder;
import khang.blinkfeed.utils.Utils;

import java.util.List;
import java.util.ArrayList;

public class BlinkFeedActivity extends ActionBarActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggler;

    @Override protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_blink_feed);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggler = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.cd_drawer_open,
                R.string.cd_drawer_close
        );

        drawerLayout.setDrawerListener(drawerToggler);

        Database.createFeed(new Feed("Tinh te", "http://tinhte.vn"));

        List<Feed> feeds = Database.getAllFeeds();
        Utils.androidLog("size of feeds="+feeds.size());
    }

    @Override protected void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // sync drawer toggle indicators
        drawerToggler.syncState();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_blink_feed, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggler.onOptionsItemSelected(item)) {
            return true;
        }
        int itemId = item.getItemId();
        if (itemId == R.id.menu_action_add) {
            Intent feedPreferenceIntent = new Intent(this, AddFeedActivity.class);
            feedPreferenceIntent.putExtra("feed", new Feed());
            startActivity(feedPreferenceIntent);
        } /*else if (itemId == R.id.menu_action_save) {

        } else if (itemId == R.id.menu_action_delete) {

        }
        */
        return super.onOptionsItemSelected(item);
    }
}