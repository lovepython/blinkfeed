package khang.blinkfeed.activity;

import android.content.res.Configuration;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import khang.blinkfeed.R;

public class BaseActivity extends ActionBarActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggler;
    private ListView drawerList;

    @Override protected void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);

        // retrieve drawer layout
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        // retrieve drawer list
        drawerList = (ListView)findViewById(R.id.left_drawer);
        drawerList.setAdapter(new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                new String[] {
                    "Java/Groovy",
                    "Python"
                }
            )
        );

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                    (String)parent.getItemAtPosition(position),
                    Toast.LENGTH_SHORT
                );
            }
        });

        drawerToggler = new ActionBarDrawerToggle(
            this,
            drawerLayout,
            R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
            R.string.cd_drawer_open,
            R.string.cd_drawer_close
        ) {
            @Override public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
            @Override public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        drawerLayout.setDrawerListener(drawerToggler);
        // actionBar setup
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerToggler.syncState();
    }

    // use ActionBarDrawerToggler we must implements
    // onOptionsItemSelected() && onConfigurationChanged()
    @Override public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        drawerToggler.onConfigurationChanged(configuration);
    }

    @Override public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (drawerToggler.onOptionsItemSelected(menuItem)) {
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}