package khang.blinkfeed;

import khang.blinkfeed.database.Database;
import android.app.Application;

public class App extends Application {
    @Override public void onCreate() {
        super.onCreate();
        Database.init(this);
    }
}