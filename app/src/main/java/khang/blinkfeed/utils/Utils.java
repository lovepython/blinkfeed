package khang.blinkfeed.utils;

import android.content.Context;
import android.widget.Toast;
import android.util.Log;

public class Utils {

    public static void sleep(int millisecs) {
        try {
            Thread.sleep(millisecs);
        } catch(InterruptedException ignored) {

        }
    }

    public static void toastMessage(Context context, String msg) {
        Toast.makeText(context.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    // simple log
    public static void androidLog(String msg) {
        Log.i("IWCJFF", msg);
    }
}