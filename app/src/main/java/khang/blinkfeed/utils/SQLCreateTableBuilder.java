package khang.blinkfeed.utils;

import java.util.Locale;

public class SQLCreateTableBuilder {
    private StringBuilder sb = new StringBuilder();

    public SQLCreateTableBuilder table(String tableName) {
        return table(tableName, false);
    }

    public SQLCreateTableBuilder table(String tableName, boolean ifNotExists) {
        String fmt = String.format(Locale.getDefault(),
            "CREATE TABLE %s%s (",
            ifNotExists? "IF NOT EXISTS ": "",
            tableName.trim()
        );
        sb.append(fmt);
        return this;
    }

    public SQLCreateTableBuilder column(String columnName, String columnType) {
        return column(columnName, columnType, null);
    }

    public SQLCreateTableBuilder column(String columnName, String columnType, String more) {
        String fmt = String.format(Locale.getDefault(),
            "%s %s%s, ",
            columnName.trim(),
            columnType.trim(),
            (more != null)? " " + more.trim(): ""
        );
        sb.append(fmt);
        return this;
    }

    @Override public String toString() {
        return sb.substring(0, sb.length()-2) + ")";
    }
}