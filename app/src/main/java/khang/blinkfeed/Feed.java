package khang.blinkfeed;

import java.util.Calendar;
import java.util.Locale;

import java.io.Serializable;

public class Feed implements Serializable {

    int _id;
    String link = "";
    String title = "";
    Calendar timeUpdate = Calendar.getInstance();
    int categoryId = 1;

    // refresh only over wifi
    boolean refreshWifi = true;

    public Feed(String link, String title) {
        this.link = link;
        this.title = title;
    }

    public Feed() { }
    @Override public String toString() {
        return String.format(
            Locale.getDefault(),
            "[title]:%s"
        );
    }

    // GETTER
    public Integer getCategoryId() {
        return categoryId;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getFeedRefreshOnlyViaWifi() {
        return refreshWifi;
    }

    public Long getUpdateTime() {
        return timeUpdate.getTimeInMillis();
    }

    // SETTER
    public void setId(int _id) {
        this._id = _id;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setRefreshOnlyViaWifi(boolean rovw) {
        this.refreshWifi = rovw;
    }

    public void setUpdateTime(Calendar calendar) {
        this.timeUpdate = calendar;
    }

    private static final long serialVersionUID = 0L;
}