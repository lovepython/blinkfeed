package khang.blinkfeed;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.List;
import java.util.ArrayList;

public class FeedProviderListAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Feed> dataItems = new ArrayList<Feed>();

    public FeedProviderListAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override public int getCount() {
        return dataItems.size();
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public Object getItem(int position) {
        return dataItems.get(position);
    }

    @Override public View getView(int position, View v, ViewGroup parent) {
        return v;
    }
}