package khang.blinkfeed.preference;

public class FeedPreference {

    /* field members */
    private String key;
    private String text1;
    private String text2;
    private Object value;
    private boolean type;

    public FeedPreference(String key, boolean singleLine, String text1, String text2, Object value) {
        this.key = key;
        this.type = singleLine;
        this.text1 = text1;
        this.text2 = text2;
        this.value = value;
    }

    // SETTER
    public void setKey(String key) {
        this.key = key;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    // GETTER
    public String getKey() {
        return key;
    }

    public boolean getType() {
        return type;
    }

    public String getText1() {
        return text1;
    }

    public String getText2() {
        return text2;
    }

    public Object getValue() {
        return value;
    }
}