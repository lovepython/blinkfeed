package khang.blinkfeed.database;

import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Locale;
import java.util.List;
import java.util.ArrayList;

import khang.blinkfeed.Feed;
import khang.blinkfeed.utils.SQLCreateTableBuilder;

public class Database extends SQLiteOpenHelper{

    static String DB_NAME = "feed_db_ctyam";
    static int DB_VERSION = 1;

    public static String TABLE_CATEGORY = "category";
    public static String TABLE_FEED = "feed";

    public static String COLUMN_ID = "_id";
    public static String COLUMN_CATEGORY_NAME = "category_name";

    public static String COLUMN_FEED_LINK = "feed_link";
    public static String COLUMN_FEED_TITLE = "feed_title";
    public static String COLUMN_FEED_CATEGORY_ID = "feed_category_id";
    public static String COLUMN_FEED_REFRESH_WIFI = "feed_refresh_wifi";
    public static String COLUMN_FEED_UPDATE_TIME = "feed_update_time";

    private static Database INSTANCE;

    public static Database init(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new Database(context);
        }
        return INSTANCE;
    }

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static SQLiteDatabase getDatabase() {
        if (INSTANCE != null) {
            return INSTANCE.getWritableDatabase();
        }
        android.util.Log.i("Database", "INSTANCE is null");
        return null;
    }

    // ================================================
    // category operators
    public static List<String> getCategories() {
        String[] catColumns = {
            COLUMN_ID,
            COLUMN_CATEGORY_NAME
        };
        Cursor cursor = getDatabase().query(TABLE_CATEGORY, catColumns, null, null, null, null, null);
        List<String> result = new ArrayList<String>();
        if (cursor.moveToFirst()) {
            do {
                String lang = cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_NAME));
                result.add(lang);
                android.util.Log.i("Database", lang);
            } while(cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public static int deleteCategory(String categoryName) {
        String fmt = String.format(Locale.getDefault(),
            "%s=?",
            COLUMN_CATEGORY_NAME
        );
        return getDatabase().delete(TABLE_CATEGORY, fmt, new String[] {categoryName});
    }

    public static long createCategory(String categoryName) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CATEGORY_NAME, categoryName);
        return getDatabase().insert(TABLE_CATEGORY, null, cv);
    }

    // ================================================
    // feed operators
    public static List<Feed> getAllFeeds() {
        String[] columns = {
            COLUMN_ID,
            COLUMN_FEED_LINK,
            COLUMN_FEED_TITLE,
            COLUMN_FEED_CATEGORY_ID,
            COLUMN_FEED_REFRESH_WIFI,
            COLUMN_FEED_UPDATE_TIME,
        };
        Cursor cursor = getDatabase().query(TABLE_FEED, columns, null, null, null, null, null);
        List<Feed> result = new ArrayList<Feed>();
        if (cursor.moveToFirst()) {
            do {
                Feed feed = new Feed();
                feed.setId(cursor.getInt(0));
                feed.setCategoryId(cursor.getInt(cursor.getColumnIndex(Database.COLUMN_FEED_CATEGORY_ID)));
                feed.setTitle(cursor.getString(cursor.getColumnIndex(Database.COLUMN_FEED_TITLE)));
                result.add(feed);
            } while(cursor.moveToNext());
        }
        return result;
    }

    public static List<Feed> getFeedsByCategoryName() {
        List<Feed> result = new ArrayList<Feed>();
        return result;
    }

    public static long createFeed(Feed feed) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_FEED_LINK, feed.getLink());
        cv.put(COLUMN_FEED_TITLE, feed.getTitle());
        cv.put(COLUMN_FEED_CATEGORY_ID, feed.getCategoryId());
        cv.put(COLUMN_FEED_REFRESH_WIFI, feed.getFeedRefreshOnlyViaWifi());
        cv.put(COLUMN_FEED_UPDATE_TIME, feed.getUpdateTime());
        return getDatabase().insert(TABLE_FEED, null, cv);
    }
    // ================================================
    // ================================================
    @Override public void onCreate(SQLiteDatabase db) {
        SQLCreateTableBuilder createTableCategory = new SQLCreateTableBuilder()
                .table(TABLE_CATEGORY)
                .column(COLUMN_ID, "INTEGER PRIMARY KEY", "autoincrement")
                .column(COLUMN_CATEGORY_NAME, "TEXT NOT NULL");

        SQLCreateTableBuilder createTableFeed = new SQLCreateTableBuilder()
                .table(TABLE_FEED)
                .column(COLUMN_ID, "INTEGER", "PRIMARY KEY autoincrement")
                .column(COLUMN_FEED_LINK, "TEXT NOT NULL")
                .column(COLUMN_FEED_TITLE, "TEXT NOT NULL")
                .column(COLUMN_FEED_CATEGORY_ID, "INTEGER NOT NULL", "default 1")
                .column(COLUMN_FEED_REFRESH_WIFI, "INTEGER NOT NULL")
                .column(COLUMN_FEED_UPDATE_TIME, "LONG", "default timestamp");

        db.execSQL(createTableCategory.toString());
        db.execSQL(createTableFeed.toString());

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CATEGORY_NAME, "Uncategorized");
        db.insert(TABLE_CATEGORY, null, cv);
    }

    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String fmt = String.format(Locale.getDefault(),
            "DROP TABLE %s",
            TABLE_CATEGORY
        );
        db.execSQL(fmt);
        onCreate(db);
    }
}