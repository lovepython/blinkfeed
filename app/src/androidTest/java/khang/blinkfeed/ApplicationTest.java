package khang.blinkfeed;

import android.app.Application;
import android.test.ApplicationTestCase;
import khang.blinkfeed.utils.SQLCreateTableBuilder;

public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    static String TABLE_FEED = "feed";
    static String COLUMN_ID = "_id";
    static String COLUMN_FEED_LINK = "feed_link";
    static String COLUMN_FEED_TITLE = "feed_title";
    static String COLUMN_FEED_CATEGORY_ID = "feed_category_id";
    static String COLUMN_FEED_REFRESH_WIFI = "feed_refresh_wifi";
    static String COLUMN_FEED_UPDATE_TIME = "feed_update_time";

    public void testSQL() {
        SQLCreateTableBuilder sqlQueryBuilder = new SQLCreateTableBuilder()
            .table(TABLE_FEED)
            .column(COLUMN_FEED_LINK, "TEXT")
            .column(COLUMN_FEED_TITLE, "TEXT");
    }
}