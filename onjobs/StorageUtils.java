package vn.icomm.corelib.util;

import android.content.Context;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.SimpleStorageConfiguration;
import com.sromku.simple.storage.Storage;

public class StorageUtils {
	
	private static final String PUBLIC_KEY = "UbOSX8PD2D3nUl31";
	private static final String PRIVATE_KEY = "for5qyoj6Evjost5";
	
	public static Storage getInternalStorage(Context context, boolean encrypted) {
		Storage storage = SimpleStorage.getInternalStorage(context);
		if (encrypted) {
			enableContentEncrypted(PUBLIC_KEY, PRIVATE_KEY);
		} else {
			disableContentEncrypted();
		}
		return storage;
	}
	
	public static Storage getExternalStorage(Context context, boolean encrypted) {
		Storage storage = SimpleStorage.getExternalStorage(context);
		if (encrypted) {
			enableContentEncrypted(PUBLIC_KEY, PRIVATE_KEY);
		} else {
			disableContentEncrypted();
		}
		return storage;
	}
	
	public static Storage getStorage(Context context, boolean encrypted) {
		if (SimpleStorage.isExternalStorageWritable()) {
			return getExternalStorage(context, encrypted);
		}
		return getInternalStorage(context, encrypted);
	}
	
	private static void enableContentEncrypted(String publicKey, String privateKey) {
		SimpleStorageConfiguration storageConf = new SimpleStorageConfiguration.Builder()
				.setEncryptContent(publicKey, privateKey)
				.build();
		SimpleStorage.updateConfiguration(storageConf);
	}
	
	private static void disableContentEncrypted() {
		SimpleStorage.updateConfiguration(new SimpleStorageConfiguration.Builder().build());
	}
	
	public static void logFile(Storage storage, String dirName, String fileName, String content) {
		if (!storage.isFileExists(dirName, fileName)) {
			storage.createDirectory(dirName, false);
			storage.createFile(dirName, fileName, content);
		} else {
			storage.appendFile(dirName, fileName, content);
		}
	}
}
