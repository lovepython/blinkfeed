package vn.icomm.corelib.errors;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;

import com.sromku.simple.storage.Storage;

import vn.icomm.corelib.util.Log;
import vn.icomm.corelib.util.StorageUtils;

public class ErrorReporter {
	
	private ThreadFiltering threadFilter;
	private Context mContext;
	
	/**
	 * Lọc thread để bắt exception
	 * 
	 * @author Rega Ng
	 *
	 */
	public static interface ThreadFiltering {
		/**
		 * Hàm trả về {@code true} nghĩa là thread sẽ được chấp nhận để báo cáo
		 * các exception xẩy ra ở thread đó.
		 * 
		 * @param thread
		 * @return
		 */
		boolean accept(Thread thread);
	}
	
	public ErrorReporter(Context context, ThreadFiltering filter) {
		threadFilter = filter;
		mContext = context;
	}
	
	public ErrorReporter(Context context) {
		this(context, null);
	}

	public void onUncaughtException(Thread thread, Throwable th) {
		if (threadFilter == null || (threadFilter != null && threadFilter.accept(thread))) {
			String stackTrace = "";
			String causedTrace = "";
			try {
				if (th != null) {
					Writer write = new StringWriter(100);
					PrintWriter printer = new PrintWriter(write);
					th.printStackTrace(printer);
					printer.flush();
					stackTrace = write.toString();
					printer.close();
				}
				Throwable caused = th.getCause();
				if (caused != null) {
					causedTrace = getCaused(caused);
				}
				report(stackTrace, causedTrace);
			} catch(Exception e) {
				Log.e("Can not handle uncaught exception");
			}
		}
	}
	
	private String getCaused(Throwable th) {
		StringBuilder sb = new StringBuilder();
		sb.append(th.toString());
		if (th.getMessage() != null) {
			sb.append(": ").append(th.getMessage());
		}
		sb.append("\n");
		StackTraceElement[] traces = th.getStackTrace();
		if (traces != null && traces.length > 0) {
			for (StackTraceElement ste : traces) {
				sb.append("\t").append(ste.getClassName()).append('.')
					.append(ste.getMethodName()).append('(')
					.append(ste.getFileName()).append(":").append(ste.getLineNumber())
					.append(')').append("\n");
			}
		}
		return sb.toString();
	}
	
	private void report(final String stackTrace, final String causedTrace) {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				if (mContext != null) {
					Storage storage = StorageUtils.getExternalStorage(mContext, false);
					SimpleDateFormat dateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String humanDatetime = dateFmt.format(new Date(System.currentTimeMillis()));
					StorageUtils.logFile(storage, "Corelib", "uncaught_exception.txt", 
							String.format("[%s] Stacktrace:\n%s\nCaused by:\n%s\n", 
									humanDatetime, stackTrace, causedTrace));
				}
			}
		}).start();
		
	}
}
