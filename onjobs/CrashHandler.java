package vn.icomm.corelib.errors;

import java.lang.Thread.UncaughtExceptionHandler;

import vn.icomm.corelib.errors.ErrorReporter.ThreadFiltering;
import android.content.Context;

public class CrashHandler implements UncaughtExceptionHandler {
	
	private UncaughtExceptionHandler mDefaultExceptionHandler;
	private ErrorReporter mErrorReporter;

	public CrashHandler(Context context, ThreadFiltering filter) {
		mDefaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(this);
		mErrorReporter = new ErrorReporter(context, filter);
	}
	
	public CrashHandler(Context context) {
		this(context, null);
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		mErrorReporter.onUncaughtException(thread, ex);
		mDefaultExceptionHandler.uncaughtException(thread, ex);
	}
}
