<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Forum</title>
    <link rel="stylesheet" href="screen.css" type="text/css" media="screen" />
</head>
<body>

<h1>Forum</h1>

<div class="content">

<?

if ( !isSet($forum) ) $forum = "";
if ( !isSet($post) ) $post = "0";
if ( !isSet($mode) ) $mode = "";
if ( !isSet($author) ) $author = "";
if ( !isSet($title) ) $title = "";
if ( !isSet($text) ) $text = "";
if ( !isSet($email) ) $email = "";
if ( !isSet($thread) ) $thread = "";
if ( !isSet($expanded) ) $expanded = "";

require_once("XPath.class.php");
require_once("class_forum.php");

$oForum = new classForum();
$oForum->setByQuery();
$oForum->process();

?>

</div>

</body>
</html>