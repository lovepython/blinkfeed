<?

class classForum
{
    var $forumName = "";
    var $thread = "";
    var $topThread = "";
    var $m_post = "";
    var $m_mode = "";
    var $m_author = "";
    var $m_email = "";
    var $m_title = "";
    var $m_text = "";
    var $m_expanded = "";
    var $m_readOnly = false;

    function setByQuery()
    {
        global $forum;
        global $thread;
        global $expanded;
        global $post;
        global $mode;
        global $author;
        global $title;
        global $text;
        global $email;

        $this->forumName = $forum;
        $this->thread = $thread;
        $this->m_post = $post;
        $this->m_mode = $mode;
        $this->m_author = $author;
        $this->m_email = $email;
        $this->m_title = $title;
        $this->m_text = $text;
        $this->m_expanded = ($expanded != "");
    }

    function process()
    {
        set_time_limit(120);

        if ($this->forumName == "")
        {
            $files = array();

            $path = "forum";
            $dir_handle = @opendir($path) or die("Unable to open $path");
            $i = 0;
            while ( $file = readdir($dir_handle))
                if ($file != "." && $file != ".." && $file != ".DAV" && $file != "archive")
                    $files[$i++] = $file;
            closedir($dir_handle);

            sort($files);
            $countFiles = count($files);
            for ($i = 0; $i < $countFiles; $i++)
                $this->printOverview($files[$i]);
        }
        else
        {
            $this->setByQuery();
            $this->processForumView();
        }
    }

    function printOverview($fileName)
    {
        $forum = subStr( $fileName, 0, strPos($fileName, ".xml") );
        $filePath = "forum/" . $fileName;
        $sThread = $this->getFileText($filePath);

        $xPath = new XPath();
        $xPath->importFromString($sThread);

        $result = $xPath->match("posts");
        $hasArchive = ($xPath->getAttribute($result[0], "archive") == "true");

        $result = $xPath->match("posts/about/title");
        $title = $xPath->getData($result[0]);

        $result = $xPath->match("posts/about/description");
        $description = $xPath->getData($result[0]);

        $newestDateXPath = "";
        $newestDate = "";
        $result = $xPath->match("posts/post/date");
        $countResult = count($result);
        for ($i = 0; $i < $countResult; $i++)
        {
            $thisDate = $xPath->getData($result[$i]);
            if ($thisDate > $newestDate || $newestDate == "")
            {
                $newestDate = $thisDate;
                $newestDateXPath = $result[$i];
            }
        }

        $results = $xPath->match($newestDateXPath . "/../author");
        $newestAuthor = $xPath->getData($results[0]);

        $results = $xPath->match($newestDateXPath . "/../date");
        $sNewestDate = $xPath->getData($results[0]);
        $sNewestDate = date( "d. F Y", strtotime($sNewestDate) );

        $results = $xPath->match($newestDateXPath . "/..");
        $newestId = $xPath->getAttributes($results[0], "id");

        ?>
        <p class="forumOverview">
            <strong><a href="index.php?forum=<?= $forum ?>"><?= $title ?></a></strong>
            <br /><?= $description ?><br />
            <em>Last post:<img src="file/date.gif" alt="" /> <?= $sNewestDate ?>
            by <?= $newestAuthor ?></em><br />
            <a href="index.php?forum=<?= $forum ?>">Current threads</a>
            <? if ($hasArchive) { ?>
            | <a href="index.php?forum=archive/<?= $forum ?>">Read-only archive</a>
            <? } ?>
        </p>
        <?
    }

    function processForumView()
    {
        $post = $this->m_post;
        $mode = $this->m_mode;
        $author = $this->m_author;
        $email = $this->m_email;
        $title = $this->m_title;
        $text = $this->m_text;

        ?><div class="posts"><?
        $filePath = "forum/" . $this->forumName . ".xml";
        $xPath = new XPath();
        $sThread = $this->getFileText($filePath);

        $xPath->importFromString($sThread);

        $result = $xPath->match("posts");
        $this->m_readOnly = ($xPath->getAttribute($result[0], "readonly") == "true");

        if ($mode == "submit" && !$this->m_readOnly)
            $sThread = $this->handleSubmit($sThread, $post, $author, $email, $title, $text, $filePath);

        $this->handleAbout($xPath);
        $this->handleFooter();
        if ($mode == "reply")
            $this->handleReply($xPath, $post);
        $this->displayPosts($xPath, 0, $post);
        ?></div><?
    }

    function displayPosts($xPath, $asReplyTo, $postToShow)
    {
        $result = $xPath->match("posts/post[@replyTo = '$asReplyTo']");
        echo "<ul>";
        $countResult = count($result);
        if ($asReplyTo == 0)
            for ($i = $countResult - 1; $i >= 0; $i--)
                $this->displayThesePosts($xPath, $result[$i], $postToShow);
        else
            for ($i = 0; $i < $countResult; $i++)
                $this->displayThesePosts($xPath, $result[$i], $postToShow);
        echo "</ul>";
    }

    function displayThesePosts($xPath, $resultI, $postToShow)
    {
        echo "<li>";
        $replyTo = $xPath->getAttributes($resultI, "replyTo");
        $thisId = $xPath->getAttributes($resultI, "id");
        $this->convertContent($xPath, $resultI, $thisId, $replyTo, $postToShow );
        if ($this->thread == $thisId || $this->thread ==  $this->topThread || $this->m_expanded)
            $this->displayPosts($xPath, $thisId, $postToShow);
        echo "</li>";
    }
    
    function convertContent($xPath, $context, $thisId, $replyTo, $postToShow)
    {
        $daysForNew = 7;

        $result = $xPath->match("$context/title");
        $title = $xPath->getData($result[0]);

        $result = $xPath->match("$context/author");
        $author = $xPath->getData($result[0]);
        if ($author == "")
            $author = "Anonymous";

        $result = $xPath->match("$context/email");
        $email = $xPath->getData($result[0]);
        if ($email != "")
            $author = "<a href=\"mailto:$email\">$author</a>";

        $result = $xPath->match("$context/date");
        $sDate = $xPath->getData($result[0]);
        $date = strftime( "%m-%d-%y", strtotime($sDate) );
        $today = date("Y-m-d 00:00:00");
        $daysDifference = (int) ( ( strtotime($today) - strtotime($sDate) ) / 86400 );
        $isNew = ($daysDifference <= $daysForNew);

        $isCurrent = ($thisId == $postToShow);
        $idString = ($isCurrent) ? " id=\"current\"" : "";
    
        ?>
        <div class="header">
            <?
            if ($replyTo == "0")
            {
                $this->topThread = $thisId;
                if (!$this->m_expanded)
                {
                    if ($thisId != $this->thread)
                    {
                        ?>
                        <a href="index.php?forum=<?= $this->forumName ?>&amp;thread=<?= $this->topThread ?>&amp;post=<?= $thisId ?>"
                                ><img src="file/plus.gif" alt="[+]" title="Open Thread" /></a>
                        <?
                    }
                    else
                    {
                        ?><a href="index.php?forum=<?= $this->forumName ?>"
                                ><img src="file/minus.gif" alt="[-]" title="Close Thread" /></a><?
                    }
                }
            }
            ?>
            <span class="title"<?= $idString ?>>
            <a href="index.php?forum=<?= $this->forumName ?>&amp;thread=<?= $this->topThread ?>&amp;post=<?= $thisId ?>">
            <img src="file/post<?= ($isNew) ? "_new" : "" ?>.gif" alt="" /><?= $title ?></a></span>
            <span class="author">&nbsp;<img src="file/author.gif" alt="" /><?= $author ?></span>
            <span class="date">&nbsp;<?= $date ?></span>
        </div>
        <?
        if ($isCurrent)
        {
            $result = $xPath->match("$context/text");
            $text = $xPath->exportAsXml($result[0]);
            $text = $this->tagLinks($text);
            $text = $this->convertFormatTags($text);
            $text = str_replace("<br/>", "<br />", $text);
            $text = str_replace("<text>", "", $text);
            $text = str_replace("</text>", "", $text);
            ?>
            <div class="text">
                <?= $text ?>
            </div>
            <div class="footer">
                <a href="index.php?forum=<?= $this->forumName ?>&amp;thread=<?= $this->topThread ?>&amp;post=<?= $thisId ?>&amp;mode=reply">+ Add reply to this post</a>
            </div>
            <?
        }
    }

    function handleFooter()
    {
        ?>
        <div class="footer">
            <? if (!$this->m_readOnly) { ?>
            <a href="index.php?forum=<?= $this->forumName ?>&amp;post=0&amp;mode=reply">
                <img src="file/new.gif" alt="" />Create new thread</a> |
            <? } ?>
            <? if (!$this->m_expanded) { ?>
                <a href="index.php?forum=<?= $this->forumName ?>&amp;expanded=true">
                    <img src="file/expanded.gif" alt="" />View expanded threads</a>
            <? } else { ?>
                <a href="index.php?forum=<?= $this->forumName ?>">
                    <img src="file/expanded.gif" alt="" />View collapsed threads</a>
            <? } ?>
        </div>
        <?
    }
    
    function handleAbout($xPath)
    {
        $result = $xPath->match("posts/about/title");
        $title = $xPath->getData($result[0]);
        $result = $xPath->match("posts/about/description");
        $description = $xPath->getData($result[0]);
        ?>
        <div class="aboutTitle"><?= $title ?></div>
        <div class="aboutDescription"><?= $description ?></div>
        <?
    }
    
    function handleSubmit($parSThread, $post, $author, $email, $title, $text, $filePath)
    {
        $sThread = $parSThread;
        $sPost = "";
        $newId = $this->getNewId($sThread);
        $sDate = $this->getIsoDate();

        $author = $this->prepareInput($author);
        $title = $this->prepareInput($title);
        $email = $this->prepareInput($email);
        $text = $this->prepareInput($text);
        $text = str_replace("\r\n", "<br />", $text);
        $text = str_replace("\r", "<br />", $text);
        $text = str_replace("\n", "<br />", $text);
           
        $sPost .= "\n<post id=\"$newId\" replyTo=\"$post\">\n";
        $sPost .= "    <title>$title</title>\n";
        $sPost .= "    <author>$author</author>\n";
        $sPost .= "    <email>$email</email>\n";
        $sPost .= "    <date>$sDate</date>\n";
        $sPost .= "    <text>$text</text>\n";
        $sPost .= "</post>";
        $sThread = str_replace("</posts>", "$sPost\n</posts>", $sThread);
    
        $this->setFileText($filePath, $sThread);
        echo "<p class=\"note\">Your post has been submitted.</p>";
        return $sThread;
    }

    function prepareInput($parText)
    {
        $text = $parText;
        $text = $this->erase_ServerSideIncludes($text);
        $text = str_replace("&", "&amp;", $text);
        $text = str_replace("\\\"", "&quot;", $text);
        $text = str_replace("\\'", "&#39;", $text);
        $text = str_replace("<", "&lt;", $text);
        $text = str_replace(">", "&gt;", $text);
    
        return $text;
    }

    function erase_ServerSideIncludes( $str )
    {
        return preg_replace( "=<!--(.|\n)*-->=U", " ", $str );
    }
    
    function getIsoDate()
    {
        $today = getdate();
        $year = $today['year'];
        $mon = $this->pad($today['mon']);
        $mday = $this->pad($today['mday']);
        $hours = $this->pad($today['hours']);
        $minutes = $this->pad($today['minutes']);
        $seconds = $this->pad($today['seconds']);
        return "$year-$mon-$mday $hours:$minutes:$seconds";
    }
    
    function pad($thisValue)
    {
        $newValue = $thisValue;
        if ( strlen($thisValue) == 1 )
            $newValue = "0" . $newValue; 
        return $newValue;
    }
    
    function getNewId($sThread)
    {
        $xPath = new XPath();
        $xPath->importFromString($sThread);
        $result = $xPath->match("posts/post");
        $highestId = 0;
        $countResult = count($result);
        for ($i = 0; $i < $countResult; $i++)
        {
            $thisId = $xPath->getAttributes($result[$i], "id");
            if ($thisId > $highestId)
                $highestId = $thisId;
        }
        return ($highestId + 1);
    }
    
    function handleReply($xPath, $post)
    {
        $title = "";
        $caption = "Create New Thread";
        if ($post != "0")
        {
            $xPath2 = new XPath();
            $result = $xPath->match("posts/post[@id = '$post']");
            $sXml = $xPath->exportAsXml($result[0]);
            $xPath2->importFromString($sXml);
            $result = $xPath2->match("post/title");
            $title = $xPath2->getData($result[0]);
            $title = "Re: $title";
            $title = str_replace("Re: Re:", "Re:", $title);
            $caption = "Add Reply";
        }
    
        ?>
        <form action="index.php" class="replyForm" method="post">
            <table class="emphasis">
                <caption><?= $caption ?></caption>
                <tr>
                    <th>Title:</th>
                    <td class="inputCell"><input type="text" name="title" size="40"
                            value="<?= $title ?>" /></td>
                </tr>
                <tr>
                    <th>Author:</th>
                    <td class="inputCell"><input type="text" name="author" size="40" /></td>
                </tr>
                <tr>
                    <th>Email<sup>*</sup>:</th>
                    <td class="inputCell"><input type="text" name="email" size="40" /></td>
                </tr>
                <tr>
                    <th>Text</th>
                    <td class="inputCell"><textarea name="text" cols="40" rows="8"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2" class="submit">
                        <input type="hidden" name="post" value="<?= $post ?>" />
                        <input type="hidden" name="forum" value="<?= $this->forumName ?>" />
                        <input type="hidden" name="mode" value="submit" />
                        <input type="submit" value="Submit" class="submit" />
                    </td>
                </tr>
            </table>
            <p class="footnote"><sup>*</sup>Optional</p>
            <p class="footnote">
            To format the text, you can use<br />
            [em]<em>emphasis</em>[/em] and [strong]<strong>strong emphasis</strong>[/strong].<br />
            Links are automatically tagged.
            </p>
        </form>
        <?
    }
    
    function getFileText($filePath)
    {
        $fileText = "";
    
        $fileArray = file($filePath);
        $countFile = count($fileArray);
        for ($i = 0; $i < $countFile; $i++)
            $fileText .= $fileArray[$i];
    
        return $fileText;
    }
    
    function setFileText($filePath, $fileText)
    {
        $writingOnly = "w";
        $fileHandle = fOpen($filePath, $writingOnly);
        fWrite($fileHandle, $fileText);
        fClose($fileHandle);
    }

    function tagLinks($str)
    {
        $pattern = '#(^|[^\"=]{1})(http://|ftp://|mailto:|news:)([^\s<>]+)([\s\n<>]|$)#sm';
        return preg_replace($pattern, "\\1<a href=\"\\2\\3\">\\2\\3</a>\\4", $str);
    }

    function convertFormatTags($parText)
    {
        $text = $parText;        
        $text = str_replace("[em]", "<em>", $text);
        $text = str_replace("[/em]", "</em>", $text);
        $text = str_replace("[strong]", "<strong>", $text);
        $text = str_replace("[/strong]", "</strong>", $text);
        $text = str_replace("[code]", "<code>", $text);
        $text = str_replace("[/code]", "</code>", $text);
        return $text;
    }
}

?>